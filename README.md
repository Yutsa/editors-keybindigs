# Editors and IDEs keybindings

## Movement

`Ctrl + f`: Move cursor one character forward

`Ctrl + b`: Move cursor one character backward

`Ctrl + p`: Move cursor one character upward

`Ctrl + n`: Move cursor one character downward


`Ctrl + e`: Move cursor to the end of the current line

`Ctrl + a`: Move cursor to the start of the current line



`Alt + f`: Move cursor one word forward

`Alt + b`: Move cursor one word backward

## Edition

`Alt + w`: Copy selection

`Ctrl + w`: Cut selection

`Ctrl + y`: Paste selection

`Ctrl + s`: Search

`Ctrl + Shift + s`: Search and replace

`Ctrl + x, Ctrl + s`: Save file

`Ctrl + x,  Ctrl + f`: Open file

## Code edition

`Alt + :`: Content assist / Auto completion

`Alt + Enter`: Quick fix

`Alt + r`: Show refactor menu

`Alt + Insert`: Generate code / Source Quick Menu (Eclipse)

`Ctrl + Shift + f`: Format code

`Alt + s`: Surround With Quick Menu

## Misc

`Alt + x`: Execute command